#!/usr/bin/env python2

#  @file plot.py
#
#  @brief Makes plots for the Sherpa internal variations.
#
#  run: ./plot.py [path to the root file with histograms]
#
#  @author Lubos Vozdecky
#  Contact: vozdeckyl@gmail.com

import ROOT
import sys
import os
from array import array

def main(histogramFilePath):
    sys.argv.append("-b")

    if not os.path.exists("{}/plots".format(histogramFilePath)):
        os.mkdir("{}/plots".format(histogramFilePath))
    
    histogramFile = ROOT.TFile("{}/histograms/hadded_all.root".format(histogramFilePath),"READ")

    regions = [
        "CRLow",
        "SR",
        "CRHigh"
    ]

    nJets = [
        "2jets",
        "3pjets"
    ]

    variables = [
        "BDT",
        "mBB",
        "pTV"
    ]
    
    QCDvars = [
        "MUR__1up",
        "MUR__1down",
        "MUF__1up",
        "MUF__1down",
        "MURMUF__1up",
        "MURMUF__1down"
    ]

    PDFsAndAlpha = [
        "MUR1_MUF1_PDF261068",
	"MUR1_MUF1_PDF261045",
	"MUR1_MUF1_PDF261004",
	"MUR1_MUF1_PDF261017",
	"MUR1_MUF1_PDF261064",
	"MUR1_MUF1_PDF261080",
	"MUR1_MUF1_PDF261057",
	"MUR1_MUF1_PDF261067",
	"MUR1_MUF1_PDF261009",
	"MUR1_MUF1_PDF261055",
	"MUR1_MUF1_PDF261052",
	"MUR1_MUF1_PDF261046",
	"MUR1_MUF1_PDF261029",
	"MUR1_MUF1_PDF261072",
	"MUR1_MUF1_PDF261062",
	"MUR1_MUF1_PDF270000",
	"MUR1_MUF1_PDF261090",
	"MUR1_MUF1_PDF261044",
	"MUR1_MUF1_PDF261043",
	"MUR1_MUF1_PDF261042",
	"MUR1_MUF1_PDF261041",
	"MUR1_MUF1_PDF261036",
	"MUR1_MUF1_PDF261065",
	"MUR1_MUF1_PDF261054",
	"MUR1_MUF1_PDF261035",
	"MUR1_MUF1_PDF261039",
	"MUR1_MUF1_PDF261033",
	"MUR1_MUF1_PDF261048",
	"MUR1_MUF1_PDF261040",
	"MUR1_MUF1_PDF261056",
	"MUR1_MUF1_PDF261032",
	"MUR1_MUF1_PDF261095",
	"MUR1_MUF1_PDF261008",
	"MUR1_MUF1_PDF261016",
	"MUR1_MUF1_PDF261023",
	"MUR1_MUF1_PDF261081",
	"MUR1_MUF1_PDF261019",
	"MUR1_MUF1_PDF261031",
	"MUR1_MUF1_PDF261020",
	"MUR1_MUF1_PDF261028",
	"MUR1_MUF1_PDF261074",
	"MUR1_MUF1_PDF261025",
	"MUR1_MUF1_PDF261030",
	"MUR1_MUF1_PDF261070",
	"MUR1_MUF1_PDF261071",
	"MUR1_MUF1_PDF261007",
	"MUR1_MUF1_PDF261061",
	"MUR1_MUF1_PDF261073",
	"MUR1_MUF1_PDF261092",
	"MUR1_MUF1_PDF261089",
	"MUR1_MUF1_PDF261085",
	"MUR1_MUF1_PDF261097",
	"MUR1_MUF1_PDF261086",
	"MUR1_MUF1_PDF261077",
	"MUR1_MUF1_PDF261088",
	"MUR1_MUF1_PDF261022",
	"MUR1_MUF1_PDF261076",
	"MUR1_MUF1_PDF261079",
	"MUR1_MUF1_PDF261094",
	"MUR1_MUF1_PDF261098",
	"MUR1_MUF1_PDF261099",
	"MUR1_MUF1_PDF261003",
	"MUR1_MUF1_PDF261091",
	"MUR1_MUF1_PDF261026",
	"MUR1_MUF1_PDF261083",
	"MUR1_MUF1_PDF261021",
	"MUR1_MUF1_PDF261082",
	"MUR1_MUF1_PDF261100",
	"MUR1_MUF1_PDF261015",
	"MUR1_MUF1_PDF261049",
	"MUR1_MUF1_PDF261014",
	"MUR1_MUF1_PDF261027",
	"MUR1_MUF1_PDF261038",
	"MUR1_MUF1_PDF261012",
	"MUR1_MUF1_PDF261087",
	"MUR1_MUF1_PDF261078",
	"MUR1_MUF1_PDF261005",
	"MUR1_MUF1_PDF261084",
	"MUR1_MUF1_PDF261037",
	"MUR1_MUF1_PDF261059",
	"MUR1_MUF1_PDF261024",
	"MUR1_MUF1_PDF261002",
	"MUR1_MUF1_PDF261060",
	"MUR1_MUF1_PDF261096",
	"MUR1_MUF1_PDF261050",
	"MUR1_MUF1_PDF261063",
	"MUR1_MUF1_PDF261075",
	"MUR1_MUF1_PDF269000",
	"MUR1_MUF1_PDF261058",
	#"MUR1_MUF1_PDF13000", # <- CT14nnlo (alternative PDF set)
	"MUR1_MUF1_PDF261001",
	"MUR1_MUF1_PDF261013",
	"MUR1_MUF1_PDF261066",
	"MUR1_MUF1_PDF261051",
	"MUR1_MUF1_PDF261034",
	"MUR1_MUF1_PDF261006",
	"MUR1_MUF1_PDF261069",
	#"MUR1_MUF1_PDF25300", # <- MMHT2014nnlo68cl (alternative PDF set)
	"MUR1_MUF1_PDF261047",
	"MUR1_MUF1_PDF261011",
	"MUR1_MUF1_PDF261053",
	"MUR1_MUF1_PDF261093",
	"MUR1_MUF1_PDF261018",
        "MUR1_MUF1_PDF261010"
    ]

    histograms = { entry.GetTitle() : histogramFile.Get(entry.GetTitle()) for entry in histogramFile.GetListOfKeys()}

    binWidth = 20
    
    for key, variation in histograms.items():
        variable = key.split("_")[-1]
        if variable == "BDT":
            binning = [x/10.0 for x in list(range(-10,11))]
        elif variable == "mBB":
            binning = [x*binWidth for x in list(range(0,51))] # [0,10,40,50,60,70,80,90,100,110,140,150,160,170,180,200,225,250,300,400,600,1000]
        elif variable == "pTV":
            binning = [x*binWidth for x in list(range(0,40))] # [75,80,85,90,95,100,110,120,150,200,250,300,400,600]
        else:
            print("Unknown variable: {} \n Exiting...".format(variable))
            exit()
        
        binningArray = array("d",binning)
        nBins = len(binning) - 1
        
        histograms[key] = histograms[key].Rebin(nBins, key, binningArray)

        """
        if variable != "BDT":
            for i in range(histograms[key].GetNbinsX()+1):
                histograms[key].SetBinContent(i, histograms[key].GetBinContent(i)/histograms[key].GetBinWidth(i))
                histograms[key].SetBinError(i, histograms[key].GetBinError(i)/histograms[key].GetBinWidth(i))
        """
    
    
    for variable in variables:
        for region in regions:
            for jet in nJets:

                canvasName = "Zhf {} {}".format(region, jet)
                canvas = ROOT.TCanvas(canvasName, canvasName, 1000, 900)
                upper = ROOT.TPad("upper", "upper", 0.0, 0.40, 1.0, 1.0)
                lower = ROOT.TPad("lower", "lower", 0.0, 0, 1.0, 0.40)
                
                upper.Draw()
                lower.Draw()

                lower.SetMargin(0.07,0.03,0.1,0.1)
                upper.SetMargin(0.07,0.03,0.1,0.1)
                
                upper.SetGridx()
                upper.SetGridy()
                lower.SetGridx()
                lower.SetGridy()

                upper.cd()
                
                ROOT.gStyle.SetOptStat(0)

                legend = ROOT.TLegend(0.67, 0.50, 0.96, 0.88)

                

                nominalHistogram = histograms["Zhf_Nominal_{}_{}_{}".format(region,jet,variable)]

                bin_values = []
                for i in range(1,nominalHistogram.GetNbinsX()):
                    bin_values.append(nominalHistogram.GetBinContent(i))

                y_max = 1.5*max(bin_values)
                
                nominalHistogram.SetAxisRange(0, y_max, "Y")
                nominalHistogram.GetXaxis().SetLabelSize(0.05)
                nominalHistogram.GetYaxis().SetLabelSize(0.05)
                
                nominalHistogram.SetLineColor(ROOT.kBlack)
                nominalHistogram.SetMarkerColor(ROOT.kBlack)
                nominalHistogram.SetFillStyle(0)
                nominalHistogram.SetLineWidth(3)
                nominalHistogram.GetXaxis().SetTitle(variable)
                nominalHistogram.GetXaxis().SetTitleSize(0.05)
                """
                if variable in ["mBB","pTV"]:
                    nominalHistogram.GetYaxis().SetTitle("Events / {}GeV".format(binWidth))
                elif variable == "BDT":
                    nominalHistogram.GetYaxis().SetTitle("Events / 0.1")
                """
                nominalHistogram.GetYaxis().SetTitleSize(0.05)
                nominalHistogram.SetTitle(canvasName)

                nominalHistogram.Draw("hist")
                
                for variation in PDFsAndAlpha:
                    histogramName = "Zhf_{}_{}_{}_{}".format(variation,region,jet,variable)
                    histograms[histogramName].SetMarkerColor(ROOT.kGray)
                    histograms[histogramName].SetLineColor(ROOT.kGray)
                    histograms[histogramName].SetFillStyle(0)
                    histograms[histogramName].SetLineStyle(1)
                    histograms[histogramName].SetLineWidth(1)
                    histograms[histogramName].Draw("hist same")
                
                
                for n, variation in enumerate(QCDvars,2):
                    histogramName = "Zhf_{}_{}_{}_{}".format(variation,region,jet,variable)
                    histograms[histogramName].SetMarkerColor(n)
                    histograms[histogramName].SetLineColor(n)
                    histograms[histogramName].SetFillStyle(0)
                    histograms[histogramName].SetLineStyle(1)
                    histograms[histogramName].SetLineWidth(2)
                    histograms[histogramName].Draw("hist same")

                    nominalHistogram.Draw("hist same")
                
                legend.AddEntry(nominalHistogram, "Nominal", "l")
                for variation in QCDvars:
                    histogramName = "Zhf_{}_{}_{}_{}".format(variation,region,jet,variable)
                    legend.AddEntry(histograms[histogramName], variation, "l")
                
                histogramName = "Zhf_{}_{}_{}_{}".format(PDFsAndAlpha[0],region,jet,variable)
                legend.AddEntry(histograms[histogramName], "PDF + #alpha_{S}", "l")
                    
                legend.Draw()
                
                lower.cd()
                
                baseline = nominalHistogram.Clone()
                baseline.Divide(nominalHistogram)
                
                
                baseline.SetFillColorAlpha(ROOT.kRed,0.6)
                baseline.SetFillStyle(3002) # 1002
                baseline.SetLineStyle(1)
                baseline.SetLineColor(0)
                baseline.SetLineWidth(1)
                baseline.SetMarkerStyle(1)
                baseline.SetMarkerSize(0)
                baseline.GetXaxis().SetLabelSize(0.07)
                baseline.GetYaxis().SetLabelSize(0.07)
                baseline.GetYaxis().SetNdivisions(10)
                baseline.SetTitle("")

                for i in range(baseline.GetNbinsX()):
                    baseline.SetBinContent(i, 1.0)
                    
                baseline.Draw("L E2")

                histo_ratios = {}

                for variation in QCDvars:
                    histogramName = "Zhf_{}_{}_{}_{}".format(variation,region,jet,variable)
                    histo_ratios[histogramName] = histograms[histogramName].Clone()
                    histo_ratios[histogramName].Divide(nominalHistogram)
                    histo_ratios[histogramName].SetTitle("")

                for variation in PDFsAndAlpha:
                    histogramName = "Zhf_{}_{}_{}_{}".format(variation,region,jet,variable)
                    histo_ratios[histogramName] = histograms[histogramName].Clone()
                    histo_ratios[histogramName].Divide(nominalHistogram)
                    histo_ratios[histogramName].SetTitle("")


                baseline.SetAxisRange(0.4, 1.6, "Y")
                baseline.GetXaxis().SetTitle("")
                baseline.GetYaxis().SetTitle("")
                
                for variation in PDFsAndAlpha:
                    histogramName = "Zhf_{}_{}_{}_{}".format(variation,region,jet,variable)
                    histo_ratios[histogramName].SetLineColor(ROOT.kGray)
                    histo_ratios[histogramName].SetMarkerColor(ROOT.kGray)
                    histo_ratios[histogramName].SetFillColorAlpha(0, 0)
                    histo_ratios[histogramName].Draw("same hist")

                for n, variation in enumerate(QCDvars,2):
                    histogramName = "Zhf_{}_{}_{}_{}".format(variation,region,jet,variable)
                    histo_ratios[histogramName].SetLineColor(n)
                    histo_ratios[histogramName].SetMarkerColor(n)
                    histo_ratios[histogramName].SetFillColorAlpha(0, 0)
                    histo_ratios[histogramName].Draw("same hist")
                
                canvas.Update()
                canvas.Draw()
                canvas.SaveAs("{}/plots/Zhf_{}_{}_{}.png".format(histogramFilePath,region,jet,variable))
                canvas.SaveAs("{}/plots/Zhf_{}_{}_{}.pdf".format(histogramFilePath,region,jet,variable))

if __name__ == "__main__":
    main(sys.argv[1])
