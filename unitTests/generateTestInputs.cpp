
#include <iostream>
#include "TTree.h"
#include "TFile.h"

int main(int atgc, char *argv[])
{
    float weight{0.0};
    double totalWeight_SR{0.0};
    double totalWeight_CRLow{0.0};
    std::map<std::string,double> SysWeights_SR;
    std::map<std::string,double> SysWeights_CRLow;
    int selectedEvents{0};
    float pTV{0.0},mLL{0.0},pTB1{0.0},mBB{0.0},mBBPreCorr{0.0};
    int nTags{0},nJ{0},FlavL1{0},FlavL2{0};
    std::vector<std::string> weightTemplate =
      {
          "MUR__1up",
          "MUR__1down",
          "MUF__1up",
          "MUF__1down",
          "MURMUF__1up",
          "MURMUF__1down",
          "MUR1_MUF1_PDF261068",
          "MUR1_MUF1_PDF261045",
          "MUR1_MUF1_PDF261004",
          "MUR1_MUF1_PDF261017",
          "MUR1_MUF1_PDF261064",
          "MUR1_MUF1_PDF261080",
          "MUR1_MUF1_PDF261057",
          "MUR1_MUF1_PDF261067",
          "MUR1_MUF1_PDF261009",
          "MUR1_MUF1_PDF261055",
          "MUR1_MUF1_PDF261052",
          "MUR1_MUF1_PDF261046",
          "MUR1_MUF1_PDF261029",
          "MUR1_MUF1_PDF261072",
          "MUR1_MUF1_PDF261062",
          "MUR1_MUF1_PDF270000",
          "MUR1_MUF1_PDF261090",
          "MUR1_MUF1_PDF261044",
          "MUR1_MUF1_PDF261043",
          "MUR1_MUF1_PDF261042",
          "MUR1_MUF1_PDF261041",
          "MUR1_MUF1_PDF261036",
          "MUR1_MUF1_PDF261065",
          "MUR1_MUF1_PDF261054",
          "MUR1_MUF1_PDF261035",
          "MUR1_MUF1_PDF261039",
          "MUR1_MUF1_PDF261033",
          "MUR1_MUF1_PDF261048",
          "MUR1_MUF1_PDF261040",
          "MUR1_MUF1_PDF261056",
          "MUR1_MUF1_PDF261032",
          "MUR1_MUF1_PDF261095",
          "MUR1_MUF1_PDF261008",
          "MUR1_MUF1_PDF261016",
          "MUR1_MUF1_PDF261023",
          "MUR1_MUF1_PDF261081",
          "MUR1_MUF1_PDF261019",
          "MUR1_MUF1_PDF261031",
          "MUR1_MUF1_PDF261020",
          "MUR1_MUF1_PDF261028",
          "MUR1_MUF1_PDF261074",
          "MUR1_MUF1_PDF261025",
          "MUR1_MUF1_PDF261030",
          "MUR1_MUF1_PDF261070",
          "MUR1_MUF1_PDF261071",
          "MUR1_MUF1_PDF261007",
          "MUR1_MUF1_PDF261061",
          "MUR1_MUF1_PDF261073",
          "MUR1_MUF1_PDF261092",
          "MUR1_MUF1_PDF261089",
          "MUR1_MUF1_PDF261085",
          "MUR1_MUF1_PDF261097",
          "MUR1_MUF1_PDF261086",
          "MUR1_MUF1_PDF261077",
          "MUR1_MUF1_PDF261088",
          "MUR1_MUF1_PDF261022",
          "MUR1_MUF1_PDF261076",
          "MUR1_MUF1_PDF261079",
          "MUR1_MUF1_PDF261094",
          "MUR1_MUF1_PDF261098",
          "MUR1_MUF1_PDF261099",
          "MUR1_MUF1_PDF261003",
          "MUR1_MUF1_PDF261091",
          "MUR1_MUF1_PDF261026",
          "MUR1_MUF1_PDF261083",
          "MUR1_MUF1_PDF261021",
          "MUR1_MUF1_PDF261082",
          "MUR1_MUF1_PDF261100",
          "MUR1_MUF1_PDF261015",
          "MUR1_MUF1_PDF261049",
          "MUR1_MUF1_PDF261014",
          "MUR1_MUF1_PDF261027",
          "MUR1_MUF1_PDF261038",
          "MUR1_MUF1_PDF261012",
          "MUR1_MUF1_PDF261087",
          "MUR1_MUF1_PDF261078",
          "MUR1_MUF1_PDF261005",
          "MUR1_MUF1_PDF261084",
          "MUR1_MUF1_PDF261037",
          "MUR1_MUF1_PDF261059",
          "MUR1_MUF1_PDF261024",
          "MUR1_MUF1_PDF261002",
          "MUR1_MUF1_PDF261060",
          "MUR1_MUF1_PDF261096",
          "MUR1_MUF1_PDF261050",
          "MUR1_MUF1_PDF261063",
          "MUR1_MUF1_PDF261075",
          "MUR1_MUF1_PDF269000",
          "MUR1_MUF1_PDF261058",
          "MUR1_MUF1_PDF13000",
          "MUR1_MUF1_PDF261001",
          "MUR1_MUF1_PDF261013",
          "MUR1_MUF1_PDF261066",
          "MUR1_MUF1_PDF261051",
          "MUR1_MUF1_PDF261034",
          "MUR1_MUF1_PDF261006",
          "MUR1_MUF1_PDF261069",
          "MUR1_MUF1_PDF25300",
          "MUR1_MUF1_PDF261047",
          "MUR1_MUF1_PDF261011",
          "MUR1_MUF1_PDF261053",
          "MUR1_MUF1_PDF261093",
          "MUR1_MUF1_PDF261018",
          "MUR1_MUF1_PDF261010"};

    std::map<std::string, float> expectedSRevents_2jets;
    std::map<std::string, float> expectedCRLowEvents_2jets;
    std::map<std::string, float> expectedCRHighEvents_2jets;
    std::map<std::string, float> expectedSRevents_3pjets;
    std::map<std::string, float> expectedCRLowEvents_3pjets;
    std::map<std::string, float> expectedCRHighEvents_3pjets;
    
    for(auto & s : weightTemplate)
    {
	expectedSRevents_2jets[s] = 0.0;
	expectedCRLowEvents_2jets[s] = 0.0;
	expectedCRHighEvents_2jets[s] = 0.0;
	expectedSRevents_3pjets[s] = 0.0;
	expectedCRLowEvents_3pjets[s] = 0.0;
	expectedCRHighEvents_3pjets[s] = 0.0;
    }
    expectedSRevents_2jets["Nominal"] = 0.0;
    expectedCRLowEvents_2jets["Nominal"] = 0.0;
    expectedCRHighEvents_2jets["Nominal"] = 0.0;
    expectedSRevents_3pjets["Nominal"] = 0.0;
    expectedCRLowEvents_3pjets["Nominal"] = 0.0;
    expectedCRHighEvents_3pjets["Nominal"] = 0.0;
    
    std::vector<std::string> * WeightNames = new std::vector<std::string>();
    std::vector<float> * weights = new std::vector<float>();
    std::string * sample = nullptr;
    std::string * description = nullptr;
    int isResolved{0};
    float BDT{0.0};
    
    
    TFile *hfile = hfile = TFile::Open("testInput.root","RECREATE");

    TTree *tree = new TTree("Nominal","Tree containing all the nominal events");
    tree->Branch("sample","string",&sample);
    tree->Branch("Description", "string", &description);
    tree->Branch("WeightNames","vector<string>",&WeightNames);
    tree->Branch("BDT",&BDT,"BDT/F");
    tree->Branch("EventWeight",&weight,"EventWeight/F");
    tree->Branch("EventWeights","vector<float>",&weights);
    tree->Branch("nTags",&nTags,"nTags/I");
    tree->Branch("mLL",&mLL,"mLL/F");
    tree->Branch("pTV",&pTV,"pTV/F");
    tree->Branch("isResolved",&isResolved,"isResolved/I");
    tree->Branch("pTB1",&pTB1,"pTB1/F");
    tree->Branch("nJ",&nJ,"nJ/I");
    tree->Branch("mBB",&mBB,"mBB/F");
    tree->Branch("mBBPreCorr",&mBBPreCorr,"mBBPreCorr/F");
    tree->Branch("FlavL1",&FlavL1,"FlavL1/I");
    tree->Branch("FlavL2",&FlavL2,"FlavL2/I");


    int numberOfEvents = 12000;

    for(int i=0;i<numberOfEvents;i++)
    {
        if(i%3==0)
        {
            description = new std::string("SR");
            weight = 1.0f;
        }
        else if(i%3==1)
        {
            description = new std::string("CRLow");
            weight = 1.0f;
        }
	else
	{
	    description = new std::string("CRHigh");
            weight = 1.0f;
	}

        // 2000 events will fail the cut
        if(i>=10000)
        {
            pTV = 10.0f;
        }
        else
        {
            pTV = 120.0f;
        }

        if(i%2==0)
        {
            sample = new std::string("Zbb");
        }
        else
        {
            sample = new std::string("Zbl");
        }

	if(i%5==0)
	{
	    nJ=2;
	}
	else if(i%5==1)
	{
	    nJ=3;
	}
	else
	{
	    nJ=4;
	}

	
        BDT = 0.5f;
        nTags = 2;
        mLL = 99.0f;
        pTB1 = 50.0f;
        mBB = 150.0f;
	mBBPreCorr = 150.0f;
        FlavL1 = 1;
        FlavL2 = 1;
        isResolved = 1;
        
        weights = new std::vector<float>();
        WeightNames = new std::vector<std::string>();


        for(const std::string & s : weightTemplate)
        {
            if(i%2 == 0 && s != "MURMUF__1down")
            {
		//events don't have the systematic var => use just the nominal weight
		if(pTV == 120.0f)
		{
		    if(*description=="SR")
		    {
			if(nJ==2)
			{
			    expectedSRevents_2jets[s] += weight;
			}
			else
			{
			    expectedSRevents_3pjets[s] += weight;
			}
		    }
		    else if(*description=="CRLow")
		    {
			if(nJ==2)
			{
			    expectedCRLowEvents_2jets[s] += weight;
			}
			else
			{
			    expectedCRLowEvents_3pjets[s] += weight;
			}
		    }
		    else if(*description=="CRHigh")
		    {
			if(nJ==2)
			{
			    expectedCRHighEvents_2jets[s] += weight;
			}
			else
			{
			    expectedCRHighEvents_3pjets[s] += weight;
			}
		    }
		}
		
                continue;
            }

            weights->push_back(2.0f);
            WeightNames->push_back(s);

	    if(pTV == 120.0f)
	    {
		if(*description=="SR")
		{
		    if(nJ==2)
		    {
			expectedSRevents_2jets[s] += 2.0f*weight;
		    }
		    else
		    {
			expectedSRevents_3pjets[s] += 2.0f*weight;
		    }
		    
		}
		else if(*description=="CRLow")
		{
		    if(nJ==2)
		    {
			expectedCRLowEvents_2jets[s] += 2.0f*weight;
		    }
		    else
		    {
			expectedCRLowEvents_3pjets[s] += 2.0f*weight;
		    }
		    
		}
		else if(*description=="CRHigh")
		{
		    if(nJ==2)
		    {
			expectedCRHighEvents_2jets[s] += 2.0f*weight;
		    }
		    else
		    {
			expectedCRHighEvents_3pjets[s] += 2.0f*weight;
		    }
		}
	    }
        }

	//log nominal
	if(pTV == 120.0f)
	{
	    if(*description=="SR")
	    {
		if(nJ==2)
		{
		    expectedSRevents_2jets["Nominal"] += weight;
		}
		else
		{
		    expectedSRevents_3pjets["Nominal"] += weight;
		}
	    }
	    else if(*description=="CRLow")
	    {
		if(nJ==2)
		{
		    expectedCRLowEvents_2jets["Nominal"] += weight;
		}
		else
		{
		    expectedCRLowEvents_3pjets["Nominal"] += weight;
		}
	    }
	    else if(*description=="CRHigh")
	    {
		if(nJ==2)
		{
		    expectedCRHighEvents_2jets["Nominal"] += weight;
		}
		else
		{
		    expectedCRHighEvents_3pjets["Nominal"] += weight;
		}
	    }
	}
	
        tree->Fill();
        
        delete weights;
        delete WeightNames;
        delete sample;
        delete description;
        weights = nullptr;
        WeightNames = nullptr;
        sample = nullptr;
        description = nullptr;
    }

    
    tree->Write();

    hfile->Close();
    
    delete hfile;

    std::cout << "Expected output:" << std::endl;
    std::cout << "var,SR_2jets,CRLow_2jets,CRHigh_2jets,SR_3pjets,CRLow_3pjets,CRHigh_3pjets" << std::endl;
    std::cout << "Nominal,"
	      << expectedSRevents_2jets["Nominal"] << ","
	      << expectedCRLowEvents_2jets["Nominal"] << ","
	      << expectedCRHighEvents_2jets["Nominal"] << ","
	      << expectedSRevents_3pjets["Nominal"] << ","
	      << expectedCRLowEvents_3pjets["Nominal"] << ","
	      << expectedCRHighEvents_3pjets["Nominal"]
	      << std::endl;
    
    for(auto & s : weightTemplate)
    {
	std::cout << s << ","
		  << expectedSRevents_2jets[s] << ","
		  << expectedCRLowEvents_2jets[s] << ","
		  << expectedCRHighEvents_2jets[s] << ","
		  << expectedSRevents_3pjets[s] << ","
		  << expectedCRLowEvents_3pjets[s] << ","
		  << expectedCRHighEvents_3pjets[s]
		  << std::endl;
    }

    return 0;
}
