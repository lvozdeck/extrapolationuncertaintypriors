/**
 * @file extrapolationUncertainties.cpp
 *
 * @brief Produced extrapolation uncertainties for Z+jets 2L
 *
 * @author Lubos Vozdecky
 * Contact: vozdeckyl@gmail.com
 *
 */

#include <iostream>
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "TList.h"
#include "TKey.h"
#include "TString.h"
#include "TH1D.h"
#include <list>
#include <map>
#include <iterator>

float abs(float x)
{
  if(x<0.0f)
  {
    return -x;
  }
  else
  {
    return x;
  }
}


int main(int argc, char *argv[]) {
    
    if(argc < 3)
    {
	std::cout << "ERROR: No enough arguments have been provided!" << std::endl;
	std::cout << "The program takes one argument:" << std::endl;
	std::cout << "1. path to the ROOT file" << std::endl;
	std::cout << "2. batch job ID" << std::endl;
	return 1;
    }
    
    
    std::string filePath = argv[1];
    std::string jobID = argv[2];
    std::string treeName = "Nominal";
    std::cout << "Path to the folder: " << filePath << std::endl;
    std::cout << "TTree name: " << treeName << std::endl;
    
    int numberOfEvents = 0;
    
    TFile* f = new TFile(filePath.c_str());
    
    TList * fileEntriesList = f->GetListOfKeys();
    
    if(fileEntriesList->First() == 0)
    {
	// ROOT file is empty
	std::cout << "Root file is empty" << std::endl;
	return 0;
    }
    else
    {
	TTree * tree = (TTree *)f->Get(treeName.c_str());
	numberOfEvents = tree->GetEntries();
	//std::cout << "Number of events: " << numberOfEvents << std::endl << std::endl;
	
	float weight{0.0};
	double totalWeight_SR_2jets{0.0};
	double totalWeight_CRLow_2jets{0.0};
	double totalWeight_CRHigh_2jets{0.0};
	double totalWeight_SR_3pjets{0.0};
	double totalWeight_CRLow_3pjets{0.0};
	double totalWeight_CRHigh_3pjets{0.0};
	std::map<std::string,double> SysWeights_SR_2jets;
	std::map<std::string,double> SysWeights_CRLow_2jets;
	std::map<std::string,double> SysWeights_CRHigh_2jets;
	std::map<std::string,double> SysWeights_SR_3pjets;
	std::map<std::string,double> SysWeights_CRLow_3pjets;
	std::map<std::string,double> SysWeights_CRHigh_3pjets;
	int selectedEvents{0};
	float pTV{0.0},mLL{0.0},pTB1{0.0},mBB{0.0},mBBPreCorr{0.0};
	int nTags{0},nJ{0},FlavL1{0},FlavL2{0};
	std::vector<std::string> weightTemplate =
	    {
		"MUR__1up",
		"MUR__1down",
		"MUF__1up",
		"MUF__1down",
		"MURMUF__1up",
		"MURMUF__1down",
		"MUR1_MUF1_PDF261068",
		"MUR1_MUF1_PDF261045",
		"MUR1_MUF1_PDF261004",
		"MUR1_MUF1_PDF261017",
		"MUR1_MUF1_PDF261064",
		"MUR1_MUF1_PDF261080",
		"MUR1_MUF1_PDF261057",
		"MUR1_MUF1_PDF261067",
		"MUR1_MUF1_PDF261009",
		"MUR1_MUF1_PDF261055",
		"MUR1_MUF1_PDF261052",
		"MUR1_MUF1_PDF261046",
		"MUR1_MUF1_PDF261029",
		"MUR1_MUF1_PDF261072",
		"MUR1_MUF1_PDF261062",
		"MUR1_MUF1_PDF270000",
		"MUR1_MUF1_PDF261090",
		"MUR1_MUF1_PDF261044",
		"MUR1_MUF1_PDF261043",
		"MUR1_MUF1_PDF261042",
		"MUR1_MUF1_PDF261041",
		"MUR1_MUF1_PDF261036",
		"MUR1_MUF1_PDF261065",
		"MUR1_MUF1_PDF261054",
		"MUR1_MUF1_PDF261035",
		"MUR1_MUF1_PDF261039",
		"MUR1_MUF1_PDF261033",
		"MUR1_MUF1_PDF261048",
		"MUR1_MUF1_PDF261040",
		"MUR1_MUF1_PDF261056",
		"MUR1_MUF1_PDF261032",
		"MUR1_MUF1_PDF261095",
		"MUR1_MUF1_PDF261008",
		"MUR1_MUF1_PDF261016",
		"MUR1_MUF1_PDF261023",
		"MUR1_MUF1_PDF261081",
		"MUR1_MUF1_PDF261019",
		"MUR1_MUF1_PDF261031",
		"MUR1_MUF1_PDF261020",
		"MUR1_MUF1_PDF261028",
		"MUR1_MUF1_PDF261074",
		"MUR1_MUF1_PDF261025",
		"MUR1_MUF1_PDF261030",
		"MUR1_MUF1_PDF261070",
		"MUR1_MUF1_PDF261071",
		"MUR1_MUF1_PDF261007",
		"MUR1_MUF1_PDF261061",
		"MUR1_MUF1_PDF261073",
		"MUR1_MUF1_PDF261092",
		"MUR1_MUF1_PDF261089",
		"MUR1_MUF1_PDF261085",
		"MUR1_MUF1_PDF261097",
		"MUR1_MUF1_PDF261086",
		"MUR1_MUF1_PDF261077",
		"MUR1_MUF1_PDF261088",
		"MUR1_MUF1_PDF261022",
		"MUR1_MUF1_PDF261076",
		"MUR1_MUF1_PDF261079",
		"MUR1_MUF1_PDF261094",
		"MUR1_MUF1_PDF261098",
		"MUR1_MUF1_PDF261099",
		"MUR1_MUF1_PDF261003",
		"MUR1_MUF1_PDF261091",
		"MUR1_MUF1_PDF261026",
		"MUR1_MUF1_PDF261083",
		"MUR1_MUF1_PDF261021",
		"MUR1_MUF1_PDF261082",
		"MUR1_MUF1_PDF261100",
		"MUR1_MUF1_PDF261015",
		"MUR1_MUF1_PDF261049",
		"MUR1_MUF1_PDF261014",
		"MUR1_MUF1_PDF261027",
		"MUR1_MUF1_PDF261038",
		"MUR1_MUF1_PDF261012",
		"MUR1_MUF1_PDF261087",
		"MUR1_MUF1_PDF261078",
		"MUR1_MUF1_PDF261005",
		"MUR1_MUF1_PDF261084",
		"MUR1_MUF1_PDF261037",
		"MUR1_MUF1_PDF261059",
		"MUR1_MUF1_PDF261024",
		"MUR1_MUF1_PDF261002",
		"MUR1_MUF1_PDF261060",
		"MUR1_MUF1_PDF261096",
		"MUR1_MUF1_PDF261050",
		"MUR1_MUF1_PDF261063",
		"MUR1_MUF1_PDF261075",
		"MUR1_MUF1_PDF269000",
		"MUR1_MUF1_PDF261058",
		"MUR1_MUF1_PDF13000",
		"MUR1_MUF1_PDF261001",
		"MUR1_MUF1_PDF261013",
		"MUR1_MUF1_PDF261066",
		"MUR1_MUF1_PDF261051",
		"MUR1_MUF1_PDF261034",
		"MUR1_MUF1_PDF261006",
		"MUR1_MUF1_PDF261069",
		"MUR1_MUF1_PDF25300",
		"MUR1_MUF1_PDF261047",
		"MUR1_MUF1_PDF261011",
		"MUR1_MUF1_PDF261053",
		"MUR1_MUF1_PDF261093",
		"MUR1_MUF1_PDF261018",
		"MUR1_MUF1_PDF261010"
	    };
	
	std::vector<std::string> regions = {
	    "SR",
	    "CRLow",
	    "CRHigh"
	};
	
	std::vector<std::string> nJets = {
	    "2jets",
	    "3pjets"
	};
	
	
	std::vector<std::string> * WeightNames = nullptr;
	std::vector<float> * weights = nullptr;
	std::string * sample = new std::string();
	std::string * description = new std::string();
	int isResolved{0};
	float BDT{0.0};
	
	// initialize histogram map
	std::map<std::string,TH1D> histograms;
	for(auto & region : regions)
	{
	    for(auto & jet : nJets)
	    {
		// histograms with
		for(auto & sys : weightTemplate)
		{
		    std::string name = "Zhf_" + sys + "_" + region + "_" + jet + "_"; 
		    histograms[name+"BDT"] = TH1D((name+"BDT").c_str(),(name+"BDT").c_str(),200,-1.0,1.0);
		    histograms[name+"mBB"] = TH1D((name+"mBB").c_str(),(name+"mBB").c_str(),1000,0.0,1000.0);
		    histograms[name+"pTV"] = TH1D((name+"pTV").c_str(),(name+"pTV").c_str(),1000,0.0,1000.0);
		}

		// nominal histograms
		std::string name = "Zhf_Nominal_" + region + "_" + jet + "_";
		histograms[name+"BDT"] = TH1D((name+"BDT").c_str(),(name+"BDT").c_str(),200,-1.0,1.0);
		histograms[name+"mBB"] = TH1D((name+"mBB").c_str(),(name+"mBB").c_str(),1000,0.0,1000.0);
		histograms[name+"pTV"] = TH1D((name+"pTV").c_str(),(name+"pTV").c_str(),1000,0.0,1000.0);
	    }
	}

	for(auto & [key, histogram] : histograms)
	{
	    histogram.Sumw2();
	}
	
	tree->SetBranchAddress("sample",&sample);
	tree->SetBranchAddress("Description",&description);
	tree->SetBranchAddress("isResolved",&isResolved);
	tree->SetBranchAddress("BDT",&BDT);
	tree->SetBranchAddress("EventWeight",&weight);
	tree->SetBranchAddress("EventWeights",&weights);
	tree->SetBranchAddress("pTV",&pTV);
	tree->SetBranchAddress("nTags",&nTags);
	tree->SetBranchAddress("mLL",&mLL);
	tree->SetBranchAddress("pTB1",&pTB1);
	tree->SetBranchAddress("nJ",&nJ);
	tree->SetBranchAddress("mBB",&mBB);
	tree->SetBranchAddress("mBBPreCorr",&mBBPreCorr);
	tree->SetBranchAddress("FlavL1",&FlavL1);
	tree->SetBranchAddress("FlavL2",&FlavL2);
	tree->SetBranchAddress("WeightNames",&WeightNames);
	
	for(const std::string & s : weightTemplate)
	{
	    SysWeights_SR_2jets[s] = 0.0;
	    SysWeights_CRLow_2jets[s] = 0.0;
	    SysWeights_CRHigh_2jets[s] = 0.0;
	    SysWeights_SR_3pjets[s] = 0.0;
	    SysWeights_CRLow_3pjets[s] = 0.0;
	    SysWeights_CRHigh_3pjets[s] = 0.0;
	}
	
	for(int i=0;i<numberOfEvents;i++)
	{
	    delete WeightNames;
	    delete weights;
	    delete sample;
	    delete description;
	    WeightNames = nullptr;
	    weights = nullptr;
	    sample = nullptr;
	    description = nullptr;
	    tree->GetEvent(i);
	    if (
		pTV>75.0f &&
		nTags==2 &&
		mLL<101.0f && mLL>81.0f &&
		pTB1>45 &&
		nJ>=2 &&
		FlavL1==FlavL2 &&
		(*sample == "Zbl" || *sample == "Zbb" || *sample == "Zbc" || *sample == "Zcc") &&
		isResolved == 1 &&
		(BDT >= -1.0f && BDT <= 1.0f)  //BDT is defined
                && mBBPreCorr > 50.0f //mBBPreCorr50Cut
		)
	    {
		std::string jet = "?jets";

		if(nJ==2)
		{
		    jet = "2jets";
		}
		else if(nJ>=3)
		{
		    jet = "3pjets";
		}
		
		
		std::map<std::string,float> weightMap;
		
		for(int n=0;n<WeightNames->size();n++)
		{
		    weightMap[WeightNames->at(n)] = weights->at(n);
		}
		
		for(const std::string & s : weightTemplate)
		{
		    
		    if(weightMap.find(s)!=weightMap.end())
		    {
			// the event has this systematic weight
			if(*description=="SR")
			{
			    if(nJ==2)
			    {
				SysWeights_SR_2jets[s] += (double) weight*abs(weightMap[s]);
			    }
			    else if(nJ>=3)
			    {
				SysWeights_SR_3pjets[s] += (double) weight*abs(weightMap[s]);
			    }
			}
			else if (*description=="CRLow")
			{
			    if(nJ==2)
			    {
				SysWeights_CRLow_2jets[s] += (double) weight*abs(weightMap[s]);
			    }
			    else if(nJ>=3)
			    {
				SysWeights_CRLow_3pjets[s] += (double) weight*abs(weightMap[s]);
			    }
			}
			else if (*description=="CRHigh")
			{
			    if(nJ==2)
			    {
				SysWeights_CRHigh_2jets[s] += (double) weight*abs(weightMap[s]);
			    }
			    else if(nJ>=3)
			    {
				SysWeights_CRHigh_3pjets[s] += (double) weight*abs(weightMap[s]);
			    }
			}
			
			// filling histograms
			std::string name = "Zhf_" + s + "_" + *description + "_" + jet + "_";
			histograms[name+"BDT"].Fill(BDT,(double) weight*abs(weightMap[s]));
			histograms[name+"mBB"].Fill(mBB,(double) weight*abs(weightMap[s]));
			histograms[name+"pTV"].Fill(pTV,(double) weight*abs(weightMap[s]));
		    }
		    else
		    {
			// the event doesn't have this sys weight
			if(*description=="SR")
			{
			    if(nJ==2)
			    {
				SysWeights_SR_2jets[s] += (double) weight;
			    }
			    else if(nJ>=3)
			    {
				SysWeights_SR_3pjets[s] += (double) weight;
			    }
			}
			else if (*description=="CRLow")
			{
			    if(nJ==2)
			    {
				SysWeights_CRLow_2jets[s] += (double) weight;
			    }
			    else if(nJ>=3)
			    {
				SysWeights_CRLow_3pjets[s] += (double) weight;
			    }
			}
			else if (*description=="CRHigh")
			{
			    if(nJ==2)
			    {
				SysWeights_CRHigh_2jets[s] += (double) weight;
			    }
			    else if(nJ>=3)
			    {
				SysWeights_CRHigh_3pjets[s] += (double) weight;
			    }
			}

			// filling histograms
			std::string name = "Zhf_" + s + "_" + *description + "_" + jet + "_";
			histograms[name+"BDT"].Fill(BDT,(double) weight);
			histograms[name+"mBB"].Fill(mBB,(double) weight);
			histograms[name+"pTV"].Fill(pTV,(double) weight);
		    }
		}
		
		if(*description=="SR")
		{
		    if(nJ==2)
		    {
			totalWeight_SR_2jets += (double) weight;
		    }
		    else if(nJ>=3)
		    {
			totalWeight_SR_3pjets += (double) weight;
		    }
		}
		else if (*description=="CRLow")
		{
		    if(nJ==2)
		    {
			totalWeight_CRLow_2jets += (double) weight;
		    }
		    else if(nJ>=3)
		    {
			totalWeight_CRLow_3pjets += (double) weight;
		    }
		}
		else if (*description=="CRHigh")
		{
		    if(nJ==2)
		    {
			totalWeight_CRHigh_2jets += (double) weight;
		    }
		    else if(nJ>=3)
		    {
			totalWeight_CRHigh_3pjets += (double) weight;
		    }
		}

		//filling nominal histograms
		std::string name = "Zhf_Nominal_" + *description + "_" + jet + "_";
		histograms[name+"BDT"].Fill(BDT,(double) weight);
		histograms[name+"mBB"].Fill(mBB,(double) weight);
		histograms[name+"pTV"].Fill(pTV,(double) weight);
		
		selectedEvents++;
	    }
	    /*
	      if(i%100000 == 0)
	      {
	      std::cout << 100*i/numberOfEvents << "%" << std::endl;
	      }
	    */
	}
	
	//std::cout << "EVENT COUNT: " << selectedEvents << std::endl;
	
	//std::cout << "* * * VAR WEIGHTS * * *" << std::endl;
	
	std::cout << "var,SR_2jets,CRLow_2jets,CRHigh_2jets,SR_3pjets,CRLow_3pjets,CRHigh_3pjets" << std::endl;
	
	std::cout << "Nominal" << ","
		  << totalWeight_SR_2jets << ","
		  << totalWeight_CRLow_2jets << ","
		  << totalWeight_CRHigh_2jets << ","
		  << totalWeight_SR_3pjets << ","
		  << totalWeight_CRLow_3pjets << ","
		  << totalWeight_CRHigh_3pjets
		  << std::endl;
	
	for(const std::string & s : weightTemplate)
	{
	    std::cout << s << ","
		      << SysWeights_SR_2jets[s] << ","
		      << SysWeights_CRLow_2jets[s] << ","
		      << SysWeights_CRHigh_2jets[s] << ","
		      << SysWeights_SR_3pjets[s] << ","
		      << SysWeights_CRLow_3pjets[s] << ","
		      << SysWeights_CRHigh_3pjets[s]
		      << std::endl;
	}
	
	TFile histFile(("histogram-"+jobID+".root").c_str(),"new");
	histFile.cd();
	

	for(auto & [key, histo] : histograms)
	{
	    histo.Write();
	}
    }
    
    f->Close();
    return 0;
}
