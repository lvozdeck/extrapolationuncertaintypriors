import sys

weights = [
    "Nominal",
    "MUR__1up",
    "MUR__1down",
    "MUF__1up",
    "MUF__1down",
    "MURMUF__1up",
    "MURMUF__1down",
    "MUR1_MUF1_PDF261068",
    "MUR1_MUF1_PDF261045",
    "MUR1_MUF1_PDF261004",
    "MUR1_MUF1_PDF261017",
    "MUR1_MUF1_PDF261064",
    "MUR1_MUF1_PDF261080",
    "MUR1_MUF1_PDF261057",
    "MUR1_MUF1_PDF261067",
    "MUR1_MUF1_PDF261009",
    "MUR1_MUF1_PDF261055",
    "MUR1_MUF1_PDF261052",
    "MUR1_MUF1_PDF261046",
    "MUR1_MUF1_PDF261029",
    "MUR1_MUF1_PDF261072",
    "MUR1_MUF1_PDF261062",
    "MUR1_MUF1_PDF270000",
    "MUR1_MUF1_PDF261090",
    "MUR1_MUF1_PDF261044",
    "MUR1_MUF1_PDF261043",
    "MUR1_MUF1_PDF261042",
    "MUR1_MUF1_PDF261041",
    "MUR1_MUF1_PDF261036",
    "MUR1_MUF1_PDF261065",
    "MUR1_MUF1_PDF261054",
    "MUR1_MUF1_PDF261035",
    "MUR1_MUF1_PDF261039",
    "MUR1_MUF1_PDF261033",
    "MUR1_MUF1_PDF261048",
    "MUR1_MUF1_PDF261040",
    "MUR1_MUF1_PDF261056",
    "MUR1_MUF1_PDF261032",
    "MUR1_MUF1_PDF261095",
    "MUR1_MUF1_PDF261008",
    "MUR1_MUF1_PDF261016",
    "MUR1_MUF1_PDF261023",
    "MUR1_MUF1_PDF261081",
    "MUR1_MUF1_PDF261019",
    "MUR1_MUF1_PDF261031",
    "MUR1_MUF1_PDF261020",
    "MUR1_MUF1_PDF261028",
    "MUR1_MUF1_PDF261074",
    "MUR1_MUF1_PDF261025",
    "MUR1_MUF1_PDF261030",
    "MUR1_MUF1_PDF261070",
    "MUR1_MUF1_PDF261071",
    "MUR1_MUF1_PDF261007",
    "MUR1_MUF1_PDF261061",
    "MUR1_MUF1_PDF261073",
    "MUR1_MUF1_PDF261092",
    "MUR1_MUF1_PDF261089",
    "MUR1_MUF1_PDF261085",
    "MUR1_MUF1_PDF261097",
    "MUR1_MUF1_PDF261086",
    "MUR1_MUF1_PDF261077",
    "MUR1_MUF1_PDF261088",
    "MUR1_MUF1_PDF261022",
    "MUR1_MUF1_PDF261076",
    "MUR1_MUF1_PDF261079",
    "MUR1_MUF1_PDF261094",
    "MUR1_MUF1_PDF261098",
    "MUR1_MUF1_PDF261099",
    "MUR1_MUF1_PDF261003",
    "MUR1_MUF1_PDF261091",
    "MUR1_MUF1_PDF261026",
    "MUR1_MUF1_PDF261083",
    "MUR1_MUF1_PDF261021",
    "MUR1_MUF1_PDF261082",
    "MUR1_MUF1_PDF261100",
    "MUR1_MUF1_PDF261015",
    "MUR1_MUF1_PDF261049",
    "MUR1_MUF1_PDF261014",
    "MUR1_MUF1_PDF261027",
    "MUR1_MUF1_PDF261038",
    "MUR1_MUF1_PDF261012",
    "MUR1_MUF1_PDF261087",
    "MUR1_MUF1_PDF261078",
    "MUR1_MUF1_PDF261005",
    "MUR1_MUF1_PDF261084",
    "MUR1_MUF1_PDF261037",
    "MUR1_MUF1_PDF261059",
    "MUR1_MUF1_PDF261024",
    "MUR1_MUF1_PDF261002",
    "MUR1_MUF1_PDF261060",
    "MUR1_MUF1_PDF261096",
    "MUR1_MUF1_PDF261050",
    "MUR1_MUF1_PDF261063",
    "MUR1_MUF1_PDF261075",
    "MUR1_MUF1_PDF269000",
    "MUR1_MUF1_PDF261058",
    "MUR1_MUF1_PDF13000",
    "MUR1_MUF1_PDF261001",
    "MUR1_MUF1_PDF261013",
    "MUR1_MUF1_PDF261066",
    "MUR1_MUF1_PDF261051",
    "MUR1_MUF1_PDF261034",
    "MUR1_MUF1_PDF261006",
    "MUR1_MUF1_PDF261069",
    "MUR1_MUF1_PDF25300",
    "MUR1_MUF1_PDF261047",
    "MUR1_MUF1_PDF261011",
    "MUR1_MUF1_PDF261053",
    "MUR1_MUF1_PDF261093",
    "MUR1_MUF1_PDF261018",
    "MUR1_MUF1_PDF261010"
]

if len(sys.argv)>1:
    randomNumber = float(sys.argv[1])
else:
    randomNumber = 1

expectedAnswerSR_2jets = {}
expectedAnswerCRLow_2jets = {}
expectedAnswerCRHigh_2jets = {}
expectedAnswerSR_3pjets = {}
expectedAnswerCRLow_3pjets = {}
expectedAnswerCRHigh_3pjets = {}

for s in weights:
    expectedAnswerSR_2jets[s] = 0.0
    expectedAnswerCRLow_2jets[s] = 0.0
    expectedAnswerCRHigh_2jets[s] = 0.0
    expectedAnswerSR_3pjets[s] = 0.0
    expectedAnswerCRLow_3pjets[s] = 0.0
    expectedAnswerCRHigh_3pjets[s] = 0.0

for n in range(1,10):
    with open("job-{}.out".format(n),"w+") as file:
        file.write("Path to the folder:\n")
        file.write("TTree name:\n")
        file.write("var,SR,CRLow\n")
        x = 0
        for s in weights:
            SRweight_2jets = (n+x)*randomNumber+10.3
            CRLowweight_2jets = 2.5*(n+x)*randomNumber+19.7
            CRHighweight_2jets = 3.5*(n+x)*randomNumber+14.3
            SRweight_3pjets = (n+x)*randomNumber+14.3
            CRLowweight_3pjets = 2.5*(n+x)*randomNumber+18.7
            CRHighweight_3pjets = 3.5*(n+x)*randomNumber+16.3
            file.write("{},{},{},{},{},{},{}\n".format(s,SRweight_2jets,CRLowweight_2jets,CRHighweight_2jets,SRweight_3pjets,CRLowweight_3pjets,CRHighweight_3pjets))
            expectedAnswerSR_2jets[s] += SRweight_2jets
            expectedAnswerCRLow_2jets[s] += CRLowweight_2jets
            expectedAnswerCRHigh_2jets[s] += CRHighweight_2jets
            expectedAnswerSR_3pjets[s] += SRweight_3pjets
            expectedAnswerCRLow_3pjets[s] += CRLowweight_3pjets
            expectedAnswerCRHigh_3pjets[s] += CRHighweight_3pjets
            x+=1


with open("expected.out","w+") as file:
    for s in weights:
        file.write("{},{:.7f},{:.7f},{:.7f},{:.7f},{:.7f},{:.7f}\n".format(s,
                                                                           expectedAnswerSR_2jets[s],
                                                                           expectedAnswerCRLow_2jets[s],
                                                                           expectedAnswerCRHigh_2jets[s],
                                                                           expectedAnswerSR_3pjets[s],
                                                                           expectedAnswerCRLow_3pjets[s],
                                                                           expectedAnswerCRHigh_3pjets[s]))
        
