binaryFiles : run.cpp unitTests/generateTestInputs.cpp
	g++ -o run  run.cpp `root-config --cflags --libs`
	g++ -o generateTree unitTests/generateTestInputs.cpp `root-config --cflags --libs`


clean:
	rm run
	rm generateTree
