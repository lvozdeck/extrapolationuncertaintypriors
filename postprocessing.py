#!/usr/bin/env python
import csv
import os
from sys import argv
from scipy.stats import sem
import math

weights = [
    "Nominal",
    "MUR__1up",
    "MUR__1down",
    "MUF__1up",
    "MUF__1down",
    "MURMUF__1up",
    "MURMUF__1down",
    "MUR1_MUF1_PDF261068",
    "MUR1_MUF1_PDF261045",
    "MUR1_MUF1_PDF261004",
    "MUR1_MUF1_PDF261017",
    "MUR1_MUF1_PDF261064",
    "MUR1_MUF1_PDF261080",
    "MUR1_MUF1_PDF261057",
    "MUR1_MUF1_PDF261067",
    "MUR1_MUF1_PDF261009",
    "MUR1_MUF1_PDF261055",
    "MUR1_MUF1_PDF261052",
    "MUR1_MUF1_PDF261046",
    "MUR1_MUF1_PDF261029",
    "MUR1_MUF1_PDF261072",
    "MUR1_MUF1_PDF261062",
    "MUR1_MUF1_PDF270000",
    "MUR1_MUF1_PDF261090",
    "MUR1_MUF1_PDF261044",
    "MUR1_MUF1_PDF261043",
    "MUR1_MUF1_PDF261042",
    "MUR1_MUF1_PDF261041",
    "MUR1_MUF1_PDF261036",
    "MUR1_MUF1_PDF261065",
    "MUR1_MUF1_PDF261054",
    "MUR1_MUF1_PDF261035",
    "MUR1_MUF1_PDF261039",
    "MUR1_MUF1_PDF261033",
    "MUR1_MUF1_PDF261048",
    "MUR1_MUF1_PDF261040",
    "MUR1_MUF1_PDF261056",
    "MUR1_MUF1_PDF261032",
    "MUR1_MUF1_PDF261095",
    "MUR1_MUF1_PDF261008",
    "MUR1_MUF1_PDF261016",
    "MUR1_MUF1_PDF261023",
    "MUR1_MUF1_PDF261081",
    "MUR1_MUF1_PDF261019",
    "MUR1_MUF1_PDF261031",
    "MUR1_MUF1_PDF261020",
    "MUR1_MUF1_PDF261028",
    "MUR1_MUF1_PDF261074",
    "MUR1_MUF1_PDF261025",
    "MUR1_MUF1_PDF261030",
    "MUR1_MUF1_PDF261070",
    "MUR1_MUF1_PDF261071",
    "MUR1_MUF1_PDF261007",
    "MUR1_MUF1_PDF261061",
    "MUR1_MUF1_PDF261073",
    "MUR1_MUF1_PDF261092",
    "MUR1_MUF1_PDF261089",
    "MUR1_MUF1_PDF261085",
    "MUR1_MUF1_PDF261097",
    "MUR1_MUF1_PDF261086",
    "MUR1_MUF1_PDF261077",
    "MUR1_MUF1_PDF261088",
    "MUR1_MUF1_PDF261022",
    "MUR1_MUF1_PDF261076",
    "MUR1_MUF1_PDF261079",
    "MUR1_MUF1_PDF261094",
    "MUR1_MUF1_PDF261098",
    "MUR1_MUF1_PDF261099",
    "MUR1_MUF1_PDF261003",
    "MUR1_MUF1_PDF261091",
    "MUR1_MUF1_PDF261026",
    "MUR1_MUF1_PDF261083",
    "MUR1_MUF1_PDF261021",
    "MUR1_MUF1_PDF261082",
    "MUR1_MUF1_PDF261100",
    "MUR1_MUF1_PDF261015",
    "MUR1_MUF1_PDF261049",
    "MUR1_MUF1_PDF261014",
    "MUR1_MUF1_PDF261027",
    "MUR1_MUF1_PDF261038",
    "MUR1_MUF1_PDF261012",
    "MUR1_MUF1_PDF261087",
    "MUR1_MUF1_PDF261078",
    "MUR1_MUF1_PDF261005",
    "MUR1_MUF1_PDF261084",
    "MUR1_MUF1_PDF261037",
    "MUR1_MUF1_PDF261059",
    "MUR1_MUF1_PDF261024",
    "MUR1_MUF1_PDF261002",
    "MUR1_MUF1_PDF261060",
    "MUR1_MUF1_PDF261096",
    "MUR1_MUF1_PDF261050",
    "MUR1_MUF1_PDF261063",
    "MUR1_MUF1_PDF261075",
    "MUR1_MUF1_PDF269000",
    "MUR1_MUF1_PDF261058",
    "MUR1_MUF1_PDF13000",
    "MUR1_MUF1_PDF261001",
    "MUR1_MUF1_PDF261013",
    "MUR1_MUF1_PDF261066",
    "MUR1_MUF1_PDF261051",
    "MUR1_MUF1_PDF261034",
    "MUR1_MUF1_PDF261006",
    "MUR1_MUF1_PDF261069",
    "MUR1_MUF1_PDF25300",
    "MUR1_MUF1_PDF261047",
    "MUR1_MUF1_PDF261011",
    "MUR1_MUF1_PDF261053",
    "MUR1_MUF1_PDF261093",
    "MUR1_MUF1_PDF261018",
    "MUR1_MUF1_PDF261010"
]

weightSumSR_2jets = {}
weightSumCRLow_2jets = {}
weightSumCRHigh_2jets = {}
weightSumSR_3pjets = {}
weightSumCRLow_3pjets = {}
weightSumCRHigh_3pjets = {}
for s in weights:
    weightSumSR_2jets[s] = 0.0
    weightSumCRLow_2jets[s] = 0.0
    weightSumCRHigh_2jets[s] = 0.0
    weightSumSR_3pjets[s] = 0.0
    weightSumCRLow_3pjets[s] = 0.0
    weightSumCRHigh_3pjets[s] = 0.0

path = argv[1].rstrip("/") + "/output/"

outputFiles = [(path + fileName) for fileName in os.listdir(path) if fileName != "job-postprocessing.out"]

for outputFile in outputFiles:
    with open(outputFile,"r") as file:
        reader = csv.reader(file)

        next(reader)
        next(reader)
        next(reader)

        for row in reader:
            weightSumSR_2jets[row[0]] += float(row[1])
            weightSumCRLow_2jets[row[0]] += float(row[2])
            weightSumCRHigh_2jets[row[0]] += float(row[3])
            weightSumSR_3pjets[row[0]] += float(row[4])
            weightSumCRLow_3pjets[row[0]] += float(row[5])
            weightSumCRHigh_3pjets[row[0]] += float(row[6]) 

def computeDoubleRatio(yields_SR, yields_CR, name):
    nominalRatio = yields_CR["Nominal"]/yields_SR["Nominal"]
    
    ## * * * * * * * * * * * * * *
    ## PDF variations
    ## * * * * * * * * * * * * * *
    print("* "*30)
    
    #using the NNPDF3.0nnlo PDF set
    def isPDFkey(key):
        return key[:16]=="MUR1_MUF1_PDF261"

    PDFvarSRYields = []
    PDFvarCRYields = []

    for PDFkey in filter(isPDFkey,weights):
        PDFvarSRYields.append(float(yields_SR[PDFkey]))
        PDFvarCRYields.append(float(yields_CR[PDFkey]))

    SRYieldMean = sum(PDFvarSRYields)/len(PDFvarSRYields)
    CRYieldMean = sum(PDFvarCRYields)/len(PDFvarCRYields)

    SRYieldMeanError = sem(PDFvarSRYields)
    CRYieldMeanError = sem(PDFvarCRYields)

    #error propagation for the ratio
    varRatio = CRYieldMean/SRYieldMean #SRYieldMean/CRLowYieldMean
    errorVarRatio = varRatio*math.sqrt((SRYieldMeanError/SRYieldMean)**2 + (CRYieldMeanError/CRYieldMean)**2) 

    PDFdoubleRatios = []
    print("PDF nominal: \t \t \t {}".format(varRatio/nominalRatio-1.0))
    print("PDF +ve var: \t \t \t {}".format((varRatio+errorVarRatio)/nominalRatio-1.0))
    print("PDF -ve var: \t \t \t {}".format((varRatio-errorVarRatio)/nominalRatio-1.0))

    PDFdoubleRatios.append(abs(varRatio/nominalRatio - 1.0))
    PDFdoubleRatios.append(abs((varRatio+errorVarRatio)/nominalRatio - 1.0))
    PDFdoubleRatios.append(abs((varRatio-errorVarRatio)/nominalRatio - 1.0))

    PDFuncertainty = max(PDFdoubleRatios)

    ## * * * * * * * * * * * * * *
    ## QCD variations
    ## * * * * * * * * * * * * * *

    QCDuncertainties = []
    for variation in ["MUR__1up","MUR__1down","MUF__1up","MUF__1down","MURMUF__1up","MURMUF__1down"]:
        uncertainty = (yields_CR[variation]/yields_SR[variation])/nominalRatio - 1.0
        print("{}: \t \t \t {}".format(variation,uncertainty))
        QCDuncertainties.append(abs(uncertainty))

    QCDuncertainty = max(QCDuncertainties)

    ## * * * * * * * * * * * * * *
    ## alpha_s variations
    ## * * * * * * * * * * * * * *

    alphaSuncertainties = []
    for variation in ["MUR1_MUF1_PDF269000","MUR1_MUF1_PDF270000"]:
        uncertainty = (yields_CR[variation]/yields_SR[variation])/nominalRatio - 1.0
        print("{}: \t \t {}".format(variation,uncertainty))
        alphaSuncertainties.append(abs(uncertainty))

    alphaSuncertainty = max(alphaSuncertainties)

    #sum all the variations in quadrature
    prior = math.sqrt(PDFuncertainty**2 + QCDuncertainty**2 + alphaSuncertainty**2)

    print("* "*30)
    print("{} = {}".format(name,prior))
    print("* "*30)

computeDoubleRatio(weightSumSR_2jets, weightSumCRLow_2jets, "CRLowSRExtrapolation_2jet")
print("\n")
computeDoubleRatio(weightSumSR_3pjets, weightSumCRLow_3pjets, "CRLowSRExtrapolation_3pjet")
print("\n")
computeDoubleRatio(weightSumSR_2jets, weightSumCRHigh_2jets, "CRHighSRExtrapolation_2jet")
print("\n")
computeDoubleRatio(weightSumSR_3pjets, weightSumCRHigh_3pjets, "CRHighSRExtrapolation_3pjet")

print("var,SR_2jets,CRLow_2jets,CRHigh_2jets,SR_3pjets,CRLow_3pjets,CRHigh_3pjets")
for s in weights:
    print("{},{:.7f},{:.7f},{:.7f},{:.7f},{:.7f},{:.7f}".format(s,
                                                                weightSumSR_2jets[s],
                                                                weightSumCRLow_2jets[s],
                                                                weightSumCRHigh_2jets[s],
                                                                weightSumSR_3pjets[s],
                                                                weightSumCRLow_3pjets[s],
                                                                weightSumCRHigh_3pjets[s]))
