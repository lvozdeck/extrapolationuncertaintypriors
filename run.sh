#!/usr/bin/env bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh > /dev/null
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt" > /dev/null

$3/../run $1 $2

