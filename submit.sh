#!/usr/bin/env bash

if [ ! -f "run" ]; then
    echo "The program has not been compiled."
    echo "Please compile it by running the make commmand first."
    exit
fi

if [ -z $1 ]; then
    echo "First argument should be the name of the output folder!";
    exit
fi

if [ -d $1 ]; then
    echo "Directory $1 already exists. Please choose a different name."
    exit
fi

SUBMIT_FOLDER=$1

chmod +x run
chmod +x postprocessing.sh
chmod +x run.sh

mkdir $SUBMIT_FOLDER
mkdir $SUBMIT_FOLDER/output
mkdir $SUBMIT_FOLDER/log
mkdir $SUBMIT_FOLDER/err

SUBMIT_PATH=$(pwd)/$SUBMIT_FOLDER

cp job-main.sub.template $SUBMIT_FOLDER/job-main.sub
cp job-final.sub.template $SUBMIT_FOLDER/job-final.sub
cp jobs.dag.template $SUBMIT_FOLDER/jobs.dag
cp inputFiles.txt $SUBMIT_FOLDER/inputFiles.txt

cd $SUBMIT_FOLDER

sed -i "s|<PATH>|$SUBMIT_PATH|g" job-main.sub
sed -i "s|<PATH>|$SUBMIT_PATH|g" job-final.sub

condor_submit_dag -batch-name extrapUncertainties jobs.dag
