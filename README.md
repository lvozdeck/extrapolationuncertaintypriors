# Priors for Extrapolation Uncertainties
This code is used for deriving the priors for the Z+jets extrapolation uncertainties using the Sherpa internal variations. The input is ROOT files with special MVA trees containing two extra branches:

1. vector of weights corresponding to different internal variations
```cpp
vector<float> EventWeights
```

2. vector containing the names of the systematic weight variations
```cpp
vector<string> WeightNames
```

The output is a file with the priors and histograms with the Sherpa internal variations. 

## Installation
Set up root:
```bash
setupATLAS
lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
```
Compile the code by running:
```bash
make
```
Before compiling the code, you should make sure `weightTemplate` in `run.cpp` contains all the variations you want to consider.Similarly `weightSumSR` in `postprocessing.py` should contain all the variations before you submit the jobs.

## Submitting jobs
The paths to the input ROOT files need to be listed in `inputFiles.txt`. The jobs are submitted by:
```bash
./submit.sh [output folder]
```
The output can be found in `[output folder]/output.txt`. The associated histograms can be found in `[output folder]/histograms/hadded_all.root`.

## Making plots
```bash
python plot.py [output folder]
```
Plots can be found in `[output folder]/output.txt`.
